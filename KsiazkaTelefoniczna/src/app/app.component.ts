import { Component } from '@angular/core';
import { PhoneBookFactory } from './helpers/PhoneBookFactory';
import { Contact } from './model/Contact';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(public phoneBookHelper:PhoneBookFactory){
    
  }
}
