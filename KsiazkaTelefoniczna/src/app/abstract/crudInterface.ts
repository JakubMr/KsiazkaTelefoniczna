import {baseModel} from '../abstract/baseModel'
import { List } from 'linqts';

export interface crudInterface{
    Add(baseModel:baseModel)
    GetAll(baseModel:baseModel):List<baseModel>
    Remove(baseModel:baseModel);
    Edit(baseModel:baseModel);
}