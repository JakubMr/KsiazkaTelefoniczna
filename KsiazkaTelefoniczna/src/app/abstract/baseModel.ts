// interfejst, który wymusza na modelach poniższe właściwości
export interface baseModel{
    Id:number;
    CreateDateTime:Date;
    Arch:boolean;
    GetTableName():string;
}