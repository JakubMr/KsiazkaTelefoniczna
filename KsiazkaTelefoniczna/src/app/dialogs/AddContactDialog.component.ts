//Dialog wykorzystywany do dodawania i edycji uzywa formbuildera
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    template: `
  <form [formGroup]="form" (ngSubmit)="submit(form)">
  <h1 mat-dialog-title>Add file</h1>
  <mat-dialog-content>
    <mat-form-field>
      <input matInput formControlName="firstName" placeholder="Imie">
    </mat-form-field>
    <br>
    <mat-form-field>
    <input matInput formControlName="lastName" placeholder="Nazwisko">
  </mat-form-field>
  <br>
  <mat-form-field>
  <input matInput formControlName="adress" placeholder="Adres">
</mat-form-field>
  <br>
  <mat-form-field>
  <input matInput formControlName="email" placeholder="Email">
</mat-form-field>
  <br>
  <mat-form-field>
  <input matInput formControlName="phone" placeholder="Nr telefonu">
</mat-form-field>
  </mat-dialog-content>
  <mat-dialog-actions>
    <button mat-button type="submit">Add</button>
    <button mat-button type="button" mat-dialog-close>Cancel</button>
  </mat-dialog-actions>
</form>
  `
})
export class AddContactDialog implements OnInit {

    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<AddContactDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
        //jestli nie ma danych to znaczy ze jest dodawanie
        if (this.data === null) {
        this.form = this.formBuilder.group({
            firstName: "",
            lastName: "",
            adress: "",
            email: "",
            phone: ""
        });
        }
        //jesli sa dane to edytowanie
        else {
            this.form = this.formBuilder.group({
                firstName:this.data.firstName,
                lastName:this.data.lastName,
                adress: this.data.adress,
                email: this.data.email,
                phone:this.data.phone
            });
        }
    }
    //zwraca wynik do miejsca gdzie zostalo otworzone
    submit(form) {
        var result = {
            firstName: form.value.firstName,
            lastName: form.value.lastName,
            adress: form.value.adress,
            email: form.value.email,
            phone: form.value.phone,
            id: this.data != null ? this.data.id : null,
            createDate: this.data != null ? this.data.createDate : null,
            calLogs: this.data != null ? this.data.calLogs : null
        }
        this.dialogRef.close(result);
    }
}