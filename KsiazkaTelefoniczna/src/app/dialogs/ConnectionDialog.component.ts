//dialog, który imituje polaczenie telefoniuczne, wykorzystuje Observatora do naliczania czasu
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from "rxjs";
import { IntervalObservable } from "rxjs/observable/IntervalObservable";

@Component({
    template: `
  <form [formGroup]="form" (ngSubmit)="submit(form)">
  <h1 mat-dialog-title>Dzwonienie</h1>
  <mat-dialog-content>
    <h2>Połączenie do: {{person}}</h2>
    <h3>Czas: {{time}} </h3>
  </mat-dialog-content>
  <mat-dialog-actions>
    <button mat-button type="submit">Zakoncz</button>
  </mat-dialog-actions>
</form>
  `
})
export class ConectionDialog implements OnInit {

    form: FormGroup;
    time: number;
    person;
    private alive: boolean;
    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<ConectionDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.alive = true;
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            firstName: "",
            lastName: "",
            adress: "",
            email: "",
            phone: ""
        });
        this.person = this.data.person,
        this.time = 0;
        this.startTime()
    }
    //zaczyna sie naliczanie sekundowe
    startTime() {
        Observable.interval(1000).takeWhile(() => this.alive).subscribe(x => {
            this.time++;
        });
    }

    submit(form) {
        this.alive = false;
        var result = {
            time: this.time,
            id: this.data.id,
            date: this.data.date
        }
        this.dialogRef.close(result);
    }
    ngOnDestroy() {
        this.alive = false; // switches your IntervalObservable off
    }
}
