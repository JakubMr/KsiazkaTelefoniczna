import { Injectable } from '@angular/core';
//Imitacja bazy danych uzywa localStorage
@Injectable()
export class LocalDatabase{
    private prefix = "PhoneBook."
    Add(name:string, object:object){// Dodaje do bazy danyc
        let prefixedName  = this.prefix + name;
        localStorage.setItem(prefixedName, JSON.stringify(object));
    }
    Remove(name:string){ // usuwa
        let prefixedName  = this.prefix + name;
        localStorage.removeItem(prefixedName);
    }
    Get(name:string){ // pobiera
        let prefixedName  = this.prefix + name;
        var item = localStorage.getItem(prefixedName);
        return JSON.parse(item);
    }
}