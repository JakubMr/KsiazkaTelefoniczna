import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { style, state, animate, transition, trigger } from '@angular/core';
import { AddContactDialog } from '../dialogs/AddContactDialog.component';
import { PhoneBookFactory } from '../helpers/PhoneBookFactory';
import { Contact } from '../model/Contact';
import { List } from 'linqts';
import { CallLog } from '../model/CallLog';
import { ConectionDialog } from '../dialogs/ConnectionDialog.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-phone-book',
  templateUrl: './phone-book.component.html',
  styleUrls: ['./phone-book.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class PhoneBookComponent implements OnInit {
  showCall = false;
  contact: Contact;
  contacts: Array<Contact>
  addDialog: MatDialogRef<AddContactDialog>;
  conectDialog:MatDialogRef<ConectionDialog>
  displayedColumns;
  dataSource: MatTableDataSource<Contact>;
  selection = new SelectionModel<Contact>(true, []);
  selected;
  constructor(private router:Router,public dialog: MatDialog, public factory: PhoneBookFactory) {
    this.contact = new Contact();
    this.load();
  }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  load() {
    this.selection.clear();
    this.contact = new Contact();
    var tmp = this.factory.GetAllContactWithCallLogs(this.contact);
    this.contact = null;
    this.contacts = tmp.Select(x =>{
      var ct = new Contact();
      ct.Adress = x.Adress;
      ct.Arch = x.Arch;
      ct.Email = x.Email;
      ct.CreateDateTime = x.CreateDateTime;
      ct.CallLogs = x.CallLogs;
      ct.Id = x.Id;
      ct.FirstName = x.FirstName;
      ct.LastName = x.LastName;
      ct.Tel = x.Tel;
      return ct;
    }).ToArray();
     for(let i = 0; i <this.contacts.length;i++){
       this.contacts[i].LastCall = this.contacts[i].GetLastCall();
     }

    this.dataSource = new MatTableDataSource<Contact>(this.contacts);
  }

  isAllSelected() {
    this.selected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return this.selected === numRows;
  }

  isOneSelected() {
    this.selected = this.selection.selected.length;
    if (this.selected === 1) {
      return true;
    }
    else return false;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    var that = this;
    this.columnDef();
    window.addEventListener("resize",this.columnDef)
  }
  columnDef() {
    if(document.body.clientWidth < 500){
      this.displayedColumns = ['select', 'person', 'tel'];
    }
    else{
      this.displayedColumns = ['select', 'person', 'tel', 'adress', 'email','lastCall'];
    }
  }
  call(){
    this.selected = this.selection.selected[0];
    this.conectDialog = this.dialog.open(ConectionDialog, {
      hasBackdrop: false,
      width: "300px",
      data:{
        person:this.selected.FirstName +" " + this.selected.LastName,
        id:this.selected.Id,
        date:new Date()
      }
    });

    this.conectDialog
      .afterClosed()
      .subscribe(result => {
        if (result != "") {
          var calLog = new CallLog()
          calLog.CallDateTime = result.date;
          calLog.ContactId = result.id;
          calLog.Time = result.time
          this.factory.Add(calLog);
          this.load();
        }
      });
   
  }

  addContact() {
    this.addDialog = this.dialog.open(AddContactDialog, {
      hasBackdrop: false,
      width: "300px"
    });

    this.addDialog
      .afterClosed()
      .subscribe(result => {
        if (result != "") {
          this.contact = new Contact();
          this.contact.Adress = result.adress;
          this.contact.Email = result.email;
          this.contact.LastName = result.lastName;
          this.contact.FirstName = result.firstName;
          this.contact.Tel = result.phone;

          this.factory.Add(this.contact);
          this.contact = null;
          this.load();
        }
      });
  }

  removeContacts(){
    this.selected = this.selection.selected;
    for(let i =0; i< this.selected.length; i++){
      this.contact = new Contact();
      this.contact.Id = this.selected[i].Id;
      this.factory.Remove(this.contact);
      this.contact = null;
    }
    this.load();
  }

  editContact() {
    this.selected = this.selection.selected[0];
    this.addDialog = this.dialog.open(AddContactDialog, {
      hasBackdrop: false,
      width: "300px",
      data:{
        id:this.selected.Id,
        lastName:this.selected.LastName,
        firstName:this.selected.FirstName,
        email:this.selected.Email,
        phone:this.selected.Tel,
        adress:this.selected.Adress,
        createDate:this.selected.CreateDateTime,
        calLogs:this.selected.CallLogs
      }
    });

    this.addDialog
      .afterClosed()
      .subscribe(result => {
        if (result != "") {
          this.contact = new Contact();
          this.contact.Id = result.id;
          this.contact.Adress = result.adress;
          this.contact.Email = result.email;
          this.contact.LastName = result.lastName;
          this.contact.FirstName = result.firstName;
          this.contact.Tel = result.phone;
          this.contact.CreateDateTime = result.createDate;
          this.contact.CallLogs = result.calLogs;

          this.factory.Edit(this.contact);
          this.contact = null;
          this.load();
        }
      });
  }
  goToHistory(){
    this.selected = this.selection.selected[0];
    this.router.navigate(["history",this.selected.Id]);
  }

}

