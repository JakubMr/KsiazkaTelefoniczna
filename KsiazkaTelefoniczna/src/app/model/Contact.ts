import {CallLog} from "../model/CallLog";
import { List } from 'linqts';
import {baseModel} from '../abstract/baseModel'

export class Contact implements baseModel{
    Id:number;
    Adress:string;
    Tel:string;
    Email:string;
    FirstName:string;
    LastName:string;
    CallLogs:List<CallLog>;
    CreateDateTime:Date;
    Arch:boolean;
    LastCall:Date;

    constructor(){
        this.CreateDateTime = new Date();
        this.Arch = false;
        this.CallLogs = new List<CallLog>();
    }

    GetLastCall():Date{
        if(this.CallLogs === undefined)
            return;
        var lastCall = this.CallLogs.OrderByDescending(x => x.CallDateTime).Select(x=> x.CallDateTime).FirstOrDefault();
        return lastCall;
    }

    GetTableName = () =>{ return "Contact"}
}

