
import {baseModel} from '../abstract/baseModel'
import { Contact } from './Contact';
export class CallLog implements baseModel{
    Id:number;
    CreateDateTime:Date;
    ContactId:number;
    CallDateTime:Date;
    Arch:boolean;
    Contact:Contact;
    Time:number;

    GetTableName = () =>{ return "History"}

    constructor(){
        this.CreateDateTime = new Date();
        this.Arch = false;
    }
}