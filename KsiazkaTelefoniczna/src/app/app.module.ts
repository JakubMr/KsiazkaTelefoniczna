import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

//utworzone przeze mnie komponenty 
import { PhoneBookComponent } from './phone-book/phone-book.component';// Lista kontaktów z mozliwosciami usuwania dodawania edycji
import { HistoryComponent } from './history/history.component';// rejestr polaczen
import {AddContactDialog} from './dialogs/AddContactDialog.component'; //formularz do dodania kontaktu
import{ConectionDialog} from './dialogs/ConnectionDialog.component'; // formularz imitujacy polaczenie

import { MatFormFieldModule } from '@angular/material/form-field';

import {
  RouterModule,
  Routes
} from '@angular/router';// routing potrzebny do przechodzenia miedzy komponentami


import { LocalDatabase } from './database/LocalDatabase';//serwis, który imituje baze danych uzywa Local storage

import { PhoneBookFactory } from './helpers/PhoneBookFactory'; // serwis, który jest odpowiedzialny za przetwarzanie danych
//pliki potrzebne do dzialania angular material
import {
  
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDialogModule,
  MatChipsModule,
  MatButtonModule,
  MatDatepickerModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatCheckboxModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';

const path: Routes = [
  { path: '', redirectTo: 'phoneBook', pathMatch: 'full' },
  { path: 'phoneBook', component: PhoneBookComponent },
  { path: 'history', component: HistoryComponent},
  { path: 'history/:id', component: HistoryComponent},
]; // okreslenie sciezek routingu

@NgModule({
  declarations: [
    AppComponent,
    PhoneBookComponent,
    HistoryComponent,
    AddContactDialog,
    ConectionDialog
  ],
  imports: [
    RouterModule.forRoot(path),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,  
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    BrowserModule,
    MatButtonModule,
    MatCheckboxModule,
  ],
  entryComponents:[ConectionDialog,AddContactDialog], // potrzebne zeby wyskakiwaly okna dialogowe
  providers: [LocalDatabase, PhoneBookFactory], //Serwisy wstrzykiwane przez angulara do konstruktorow
  bootstrap: [AppComponent]
})
export class AppModule { }
