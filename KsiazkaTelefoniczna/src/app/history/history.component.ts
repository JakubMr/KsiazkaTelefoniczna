import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { style, state, animate, transition, trigger } from '@angular/core';
import { AddContactDialog } from '../dialogs/AddContactDialog.component';
import { PhoneBookFactory } from '../helpers/PhoneBookFactory';
import { Contact } from '../model/Contact';
import { List } from 'linqts';
import { CallLog } from '../model/CallLog';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  callLogs: Array<CallLog>
  displayedColumns = ['select','person','tel','date','time'];
  selected;
  sub:any;
  contactId:number;
  callLog:CallLog
  dataSource: MatTableDataSource<CallLog>;
  selection = new SelectionModel<CallLog>(true, []);
  constructor(private route:ActivatedRoute, public factory:PhoneBookFactory) {
    this.callLog  = new CallLog();
    this.sub = this.route.params.subscribe(params => {
      var tmp = +params['id'];
      if(tmp > 0)
        this.contactId = tmp;
     });

    this.load();
   }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
  
  load() {
    this.selection.clear();
    var tmp;    
    this.callLog = new CallLog();
    if(this.contactId > 0){
      this.callLog.ContactId = this.contactId;
      tmp = this.factory.GetCallLogsForContact(this.callLog)}
    else
      tmp = this.factory.GetAllCallLogsWithContact(this.callLog)
    
    this.callLog = null;
    this.callLogs = tmp.Select(x =>{
      var cl = new CallLog();
      cl.Arch = x.Arch;
      cl.CallDateTime = x.CallDateTime;
      cl.Contact = x.Contact;
      cl.CreateDateTime = x.CreateDateTime;
      cl.Id = x.Id;
      cl.Time = x.Time;
      return cl;
    }).ToArray();

    this.dataSource = new MatTableDataSource<CallLog>(this.callLogs);
  }

  isAllSelected() {
    this.selected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return this.selected === numRows;
  }

  isOneSelected() {
    this.selected = this.selection.selected.length;
    if (this.selected === 1) {
      return true;
    }
    else return false;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  removeContacts(){
    this.selected = this.selection.selected;
    for(let i =0; i< this.selected.length; i++){
      this.callLog = new CallLog();
      this.callLog.Id = this.selected[i].Id;
      this.factory.Remove(this.callLog);
      this.callLog = null;
    }
    this.load();
  }

  ngOnInit() {
  }

}
