import { Injectable } from '@angular/core';
import { LocalDatabase } from '../database/LocalDatabase';
import { crudInterface } from '../abstract/crudInterface';
import { Contact } from '../model/Contact';
import { List } from 'linqts';
import { baseModel } from '../abstract/baseModel';
import { CallLog } from '../model/CallLog';
import { forEach } from '@angular/router/src/utils/collection';

// Serwis, który odpowiada za przetwarzanie danych wykorzystuje biblioteke Linqu
@Injectable()
export class PhoneBookFactory implements crudInterface {
    ActiveContact:Contact
    constructor(public dataBase: LocalDatabase) {

    }

    Add(model: baseModel) {
        var all = this.GetAll(model);
        var lastId = all.OrderByDescending(x => x.CreateDateTime).Select(x => x.Id).FirstOrDefault();
        if (lastId == null || lastId == undefined)
            lastId = 0;
        lastId = lastId + 1;
        model.Id = lastId++;
        all.Add(model);

        this.dataBase.Add(model.GetTableName(), all)
    }

    GetAll(model: baseModel): List<baseModel> {
        var newTable = new List<baseModel>();
        var tmp = this.dataBase.Get(model.GetTableName())
        if (tmp == undefined || tmp == null) {
            this.dataBase.Add(model.GetTableName(), newTable)
            return newTable
        }
        else {
            for (let i = 0; i < tmp._elements.length; i++) {
                newTable.Add(tmp._elements[i]);
            }
            return newTable;
        }
    }

    GetAllContactWithCallLogs(model: baseModel): List<Contact> {
        var newTable = new List<Contact>();
        this.InitTable(model, newTable);
        var tmp = this.dataBase.Get(model.GetTableName())

        newTable = newTable.Where(x =>x.Arch === false).ToList();
        newTable.ForEach(x => {
            var tmpModel = new CallLog();
            tmpModel.ContactId = x.Id;
            x.CallLogs = this.GetCallLogsForContact(tmpModel);
        })

        return newTable;
    }

    InitTable(model:baseModel, newTable:any){
        var tmp = this.dataBase.Get(model.GetTableName())
        if (tmp == undefined || tmp == null) {
            this.dataBase.Add(model.GetTableName(), newTable)
        }
        else {
            for (let i = 0; i < tmp._elements.length; i++) {
                newTable.Add(tmp._elements[i]);
            }
        }
    }

    GetCallLogsForContact(model: CallLog): List<CallLog> {

        var newTable = new List<CallLog>();
        this.InitTable(model, newTable);
        var tmp = newTable.Where(x => x.ContactId == model.ContactId && x.Arch ==false).ToList();
        tmp.ForEach(x=>{
            var ct = new Contact()
            ct.Id = x.ContactId;
            x.Contact = this.GetOne(ct)
        })
        return tmp.ToList();
    }

    GetOne(model:baseModel):any{
        var newTable = new List<baseModel>();
        var tmp = this.dataBase.Get(model.GetTableName())
        if (tmp == undefined || tmp == null) {
            this.dataBase.Add(model.GetTableName(), newTable)
            return null;
        }
        else {
            for (let i = 0; i < tmp._elements.length; i++) {
                newTable.Add(tmp._elements[i]);
            } 
            return newTable.Where(x=>x.Id ==model.Id).FirstOrDefault();
        }
    }
    
    GetAllCallLogsWithContact(model: CallLog): List<CallLog> {
        var newTable = new List<CallLog>();
        this.InitTable(model, newTable);

        var tableWithContact = new List<Contact>();
        this.InitTable(new Contact(), tableWithContact);

        
        newTable.ForEach(x => x.Contact = tableWithContact.Where(y =>y.Id == x.ContactId).FirstOrDefault())
        return newTable.Where(x => x.Arch == false).ToList();
    }

    Remove(model: baseModel) {
        var all = this.GetAll(model);

        var delted = all.Where(x => x.Id == model.Id).FirstOrDefault();
        delted.Arch = true;

        this.dataBase.Add(model.GetTableName(), all)
    }

    Edit(model: baseModel) {
        var all = this.GetAll(model);
        var edited = all.FirstOrDefault(x => x.Id == model.Id);
        all.Remove(edited);
        all.Add(model);
        this.dataBase.Add(model.GetTableName(), all);
    }
}